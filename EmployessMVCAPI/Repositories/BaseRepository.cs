﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

public class BaseRepository
{
    private readonly HttpClient _httpClient;

    public BaseRepository()
    {
        IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new Uri(configuration["Urls:employess"]);
    }

    public virtual async Task<string> GetEmployeesAsync()
    {
        try
        {
            HttpResponseMessage response = await _httpClient.GetAsync("employees");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                Console.WriteLine($"Error: {response.StatusCode}");
                return null;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception: {ex.Message}");
            return null;
        }
    }

    public virtual async Task<string> GetEmployeeByIdAsync(int employeeId)
    {
        try
        {
            HttpResponseMessage response = await _httpClient.GetAsync($"employee/{employeeId}");

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                Console.WriteLine($"Error: {response.StatusCode}");
                return null;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception: {ex.Message}");
            return null;
        }
    }
}
