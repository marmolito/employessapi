﻿using EmployessMVCAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace EmployessMVCAPI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
			Validation validate= new Validation()
            {
                showMessage = false,  
            };

			return View(validate);
        }
        //probando
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
