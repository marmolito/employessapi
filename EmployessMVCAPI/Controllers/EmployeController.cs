﻿using EmployessMVCAPI.Models;
using EmployessMVCAPI.Models.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EmployessMVCAPI.Controllers
{
	public class EmployeController : Controller
	{

		private readonly BaseRepository _baseRepository;
		public EmployeController()
		{
			_baseRepository = new BaseRepository();

		}
		public IActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> GetEmployessAsync(string id)
		{
			Validation validate = new Validation()
			{
				showMessage = true,
			};

			try
			{
				int valueId = (int)((id == null) ? 0 : Int64.Parse(id));
				string response = valueId > 0
				? await _baseRepository.GetEmployeeByIdAsync(Int32.Parse(id))
				: await _baseRepository.GetEmployeesAsync();

				if (response != null)
				{
					if (valueId > 0)
					{
						var employessResponse = JsonConvert.DeserializeObject<EmployeDto>(response);
						if (employessResponse.data != null)
						{
							return View("~/Views/Employe/IndexSingle.cshtml", employessResponse.data);
						}
						else
						{
							return View("~/Views/Home/Index.cshtml", validate);
						}
					}
					else
					{
						var employeeResponse = JsonConvert.DeserializeObject<EmployessDto>(response);
						return View("~/Views/Employe/Index.cshtml", employeeResponse.data);
					}
				}
				else
				{
					return View("~/Views/Home/Index.cshtml", validate);
				}
			}
			catch (Exception ex)
			{
				validate.showMessage = true;
				return View("~/Views/Home/Index.cshtml", validate);
			}


		}

	}

}
