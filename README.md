
# Challenge

This web aplication was created with considerations from the .Net Software Developer Test.
To be excecuted in local, just you must to cone the repository in your environment and run.




## Clonning the project

To clone the project, run the following command on your directory of preference.

```bash
  git clone https://gitlab.com/marmolito/employessapi.git
```



## API Reference

#### Get all Employess

```http
  GET http://dummy.restapiexample.com/api/v1/employees
```

#### Get Employee by Id

```http
  GET http://dummy.restapiexample.com/api/v1/employee/1
```

#### change the Id (1, 2, ...)


## Features

The aplication web resolves the needing to find employess and list the information in an orderly way. The next features are integrated in the project:

- Search all employess 
- Search by employee id
- Validate valid/invalid employee id
- Calculate the anual salary of employess 
- Show the employess information


## Related

Here are some considerations to be present at the project.

About the Application Requirements of the project, this have with a MVC application, data access layer to consume the APIS, business logic layer to calculate the value of anual salary, a WebApi Controller with methods to return employees list and employee data by id, a view to show employee information, Object Orientation Programming principles, environment variables and unit tests.


## Environment Variables

To run this project, you will need to add the following environment variables to your .appsettings.json file

`Urls`

-`employess`: http://dummy.restapiexample.com/api/v1/


## Development kit

To make this project real, it was necesary many differents tools to development like:

- .Net CORE 6.0
- MVC.NET With Blazor template
- XUnit 
- Bootstrap


