using EmployessMVCAPI.Controllers;
using EmployessMVCAPI.Models.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace EmployeeMVCAPITests
{
	public class EmployeeMVCAPITest
	{
		[Fact]
		public async Task GetEmployeesAsync_ReturnsViewResult()
		{
			var mockBaseRepository = new Mock<BaseRepository>();
			mockBaseRepository.Setup(repo => repo.GetEmployeesAsync())
							.ReturnsAsync("{\"status\":\"success\",\"data\":[{\"id\":1,\"employee_name\":\"Tiger Nixon\",\"employee_salary\":50000,\"employee_age\":30,\"profile_image\":\"\"}],\"message\":\"Successfully! All records has been fetched.\"}");

			var controller = new EmployeController();
			var result = await controller.GetEmployessAsync(null);
			var viewResult = Assert.IsType<ViewResult>(result);
			Assert.NotNull(viewResult);
			Assert.Equal("~/Views/Employe/Index.cshtml", viewResult.ViewName);

			var model = viewResult.Model as List<Data>;
			Assert.NotNull(model);
			Assert.Equal(24, model.Count);
			Assert.Equal(1, model[0].id);
			Assert.Equal("Tiger Nixon", model[0].employee_name);
			Assert.Equal(320800, model[0].employee_salary);
			Assert.Equal("~/Views/Employe/Index.cshtml", viewResult.ViewName);
		}

		[Fact]
		public async Task GetEmployeeByIdAsync_ReturnsViewResult()
		{
			var mockBaseRepository = new Mock<BaseRepository>();
			mockBaseRepository.Setup(repo => repo.GetEmployeeByIdAsync(It.IsAny<int>()));

			var controller = new EmployeController();
			var result = await controller.GetEmployessAsync("1") as ViewResult;
			Assert.NotNull(result);
			Assert.Equal("~/Views/Employe/IndexSingle.cshtml", result.ViewName);

			var model = result.Model as Data;
			Assert.NotNull(model);
			Assert.Equal(1, model.id);
			Assert.Equal("Tiger Nixon", model.employee_name);
			Assert.Equal(320800, model.employee_salary);
		}

	}

}